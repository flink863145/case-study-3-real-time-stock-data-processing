package src.main.java.org.example;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Stock {
  public static void main(String[] args) throws Exception {
    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    WatermarkStrategy < Tuple5 < String, String, String, Double, Integer >> ws =
      WatermarkStrategy. < Tuple5 < String, String, String, Double, Integer >> forMonotonousTimestamps()
      .withTimestampAssigner((event, timestamp) -> {
        try {
          Timestamp ts = new Timestamp(sdf.parse(event.f0 + " " + event.f1).getTime());
          return ts.getTime();
        } catch (Exception e) {
          e.printStackTrace();
        }
        // should never reach here ...
        return 0L;
      });

    DataStream < Tuple5 < String, String, String, Double, Integer >> data = env.readTextFile("/home/myvm/Flink/Case Study 3 -  Real-Time stock data processing/src/main/resources/FUTURES_TRADES.txt")
      .map(new MapFunction < String, Tuple5 < String, String, String, Double, Integer >> () {
        public Tuple5 < String, String, String, Double, Integer > map(String value) {
          String[] words = value.split(",");
          // date,    time,     Name,       trade,                      volume
          return new Tuple5 < String, String, String, Double, Integer > (words[0], words[1], "XYZ", Double.parseDouble(words[2]), Integer.parseInt(words[3]));
        }
      })
      .assignTimestampsAndWatermarks(ws);

    // Compute per window statistics
    DataStream < String > change = data.keyBy(new KeySelector < Tuple5 < String, String, String, Double, Integer > , String > () {
        public String getKey(Tuple5 < String, String, String, Double, Integer > value) {
          return value.f2;
        }
      })//assigning 1 minute window
      .window(TumblingEventTimeWindows.of(Time.minutes(1)))
      .process(new TrackChange());

    change.addSink(StreamingFileSink
      .forRowFormat(new Path("/home/myvm/Flink/Case Study 3 -  Real-Time stock data processing/src/main/resources/Ist report.txt"),
        new SimpleStringEncoder < String > ("UTF-8"))
      .withRollingPolicy(DefaultRollingPolicy.builder().build())
      .build());



    // Alert when price change from one window to another is more than threshold
    DataStream < String > largeDelta = data.keyBy(new KeySelector < Tuple5 < String, String, String, Double, Integer > , String > () {
        public String getKey(Tuple5 < String, String, String, Double, Integer > value) {
          return value.f2;
        }
      })
      .window(TumblingEventTimeWindows.of(Time.minutes(5)))
      .process(new TrackLargeDelta(5));

    largeDelta.addSink(StreamingFileSink
      .forRowFormat(new Path("/home/myvm/Flink/Case Study 3 -  Real-Time stock data processing/src/main/resources/Alert.txt"),
        new SimpleStringEncoder < String > ("UTF-8"))
      .withRollingPolicy(DefaultRollingPolicy.builder().build())
      .build());

    env.execute("Stock Analysis");
  }


}