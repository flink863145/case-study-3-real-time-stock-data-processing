package src.main.java.org.example;

import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

public class TrackChange extends ProcessWindowFunction<Tuple5< String, String, String, Double, Integer >, String, String, TimeWindow> {
    private transient ValueState< Double > prevWindowMaxTrade;
    private transient ValueState < Integer > prevWindowMaxVol;

    public void process(String key, Context context, Iterable < Tuple5 < String, String, String, Double, Integer >> input, Collector< String > out) throws Exception {
        String windowStart = "";
        String windowEnd = "";
        Double windowMaxTrade = 0.0; // 0
        Double windowMinTrade = 0.0; // 106
        Integer windowMaxVol = 0;
        Integer windowMinVol = 0; // 348746

        for (Tuple5 < String, String, String, Double, Integer > element: input)
        //  06/10/2010, 08:00:00, 106.0, 348746
        //  06/10/2010, 08:00:00, 105.0, 331580
        {
            if (windowStart.isEmpty()) {
                windowStart = element.f0 + ":" + element.f1; // 06/10/2010 : 08:00:00
                windowMinTrade = element.f3;
                windowMinVol = element.f4;
            }
            if (element.f3 > windowMaxTrade)
                windowMaxTrade = element.f3;

            if (element.f3 < windowMinTrade)
                windowMinTrade = element.f3;

            if (element.f4 > windowMaxVol)
                windowMaxVol = element.f4;
            if (element.f4 < windowMinVol)
                windowMinVol = element.f4;

            windowEnd = element.f0 + ":" + element.f1;
        }

        Double maxTradeChange = 0.0;
        Double maxVolChange = 0.0;

        if (prevWindowMaxTrade.value() != 0) {
            maxTradeChange = ((windowMaxTrade - prevWindowMaxTrade.value()) / prevWindowMaxTrade.value()) * 100;
        }
        if (prevWindowMaxVol.value() != 0)
            maxVolChange = ((windowMaxVol - prevWindowMaxVol.value()) * 1.0 / prevWindowMaxVol.value()) * 100;

        out.collect(windowStart + " - " + windowEnd + ", " + windowMaxTrade + ", " + windowMinTrade + ", " + String.format("%.2f", maxTradeChange) +
                ", " + windowMaxVol + ", " + windowMinVol + ", " + String.format("%.2f", maxVolChange));

        prevWindowMaxTrade.update(windowMaxTrade);
        prevWindowMaxVol.update(windowMaxVol);
    }

    public void open(Configuration config) {
        prevWindowMaxTrade =
                getRuntimeContext().getState(new ValueStateDescriptor< Double >("prev_max_trade", BasicTypeInfo.DOUBLE_TYPE_INFO));

        prevWindowMaxVol = getRuntimeContext().getState(new ValueStateDescriptor < Integer > ("prev_max_vol", BasicTypeInfo.INT_TYPE_INFO));
    }
}