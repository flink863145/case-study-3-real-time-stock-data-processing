package src.main.java.org.example;

import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

public class TrackLargeDelta extends ProcessWindowFunction<Tuple5< String, String, String, Double, Integer >, String, String, TimeWindow> {
    private final double threshold;
    private transient ValueState< Double > prevWindowMaxTrade;

    public TrackLargeDelta(double threshold) {
        this.threshold = threshold;
    }

    public void process(String key, Context context, Iterable < Tuple5 < String, String, String, Double, Integer >> input, Collector< String > out) throws Exception {
        Double prevMax = 0.0;
        if (prevWindowMaxTrade.value() != null) {
            prevMax = prevWindowMaxTrade.value();
        }
        Double currMax = 0.0;
        String currMaxTimeStamp = "";

        for (Tuple5 < String, String, String, Double, Integer > element: input) {
            if (element.f3 > currMax) {
                currMax = element.f3;
                currMaxTimeStamp = element.f0 + ":" + element.f1;
            }
        }

        // check if change is more than specified threshold
        Double maxTradePriceChange = ((currMax - prevMax) / prevMax) * 100;

        if (prevMax != 0 && // don't calculate delta the first time
                Math.abs((currMax - prevMax) / prevMax) * 100 > threshold) {
            out.collect("Large Change Detected of " + String.format("%.2f", maxTradePriceChange) + "%" + " (" + prevMax + " - " + currMax + ") at  " + currMaxTimeStamp);
        }
        prevWindowMaxTrade.update(currMax);
    }

    public void open(Configuration config) {
        ValueStateDescriptor< Double > descriptor = new ValueStateDescriptor < Double > ("prev_max", BasicTypeInfo.DOUBLE_TYPE_INFO);
        prevWindowMaxTrade = getRuntimeContext().getState(descriptor);
    }
}